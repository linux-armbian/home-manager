#!/bin/bash

sudo apt -y install tmux

sh <(curl -L https://nixos.org/nix/install) --no-daemon
sleep 60

nixinstall=". $HOME/.nix-profile/etc/profile.d/nix.sh"
sleep 2
echo $nixinstall >> $HOME/.bashrc
sleep 1

tmux new -d "nix-channel --add https://github.com/nix-community/home-manager/archive/master.tar.gz home-manager ; sleep 3 ; nix-channel --update"
sleep 7
tmux new -d "nix-shell '<home-manager>' -A install"
sleep 180

sudo apt -y remove --purge tmux 
sudo apt autoremove -y
echo "Success!! Happy Use Home-Manager"

